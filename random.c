/*
 * random.c - generate a pseudo-random positive integer
 * Copyright (C) 2018 Tom Ient
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <ctype.h>

int main(int argc, char *argv[])
{
	// error handling
	if (argc != 3) {
		fprintf(stderr, "Two arguments must be given.\n");
		exit(1);
	}

	// set the seed to posix time
	srand(time(0));

	// throw an error if any of the arguments contain non-digits
	for (int i=1; i <= 2; i++) {
		for (int j=0; argv[i][j] != 0; j++) {
			if (!isdigit(argv[i][j])) {
				fprintf(stderr, "Arguments must be positive integers.\n");
				exit(1);
			}
		}
	}

	// define minimum and maximum values (inclusive)
	int minimum = atoi(argv[1]);
	int maximum = atoi(argv[2]);

	// fix up the numbers a bit
	maximum -= minimum;
	maximum++;

	// throw an error if the maximum is smaller than the minimum
	if (maximum < minimum) {
		fprintf(stderr, "Maximum must be >= minimum.\n");
		exit(1);
	}

	// throw an error if the maximum is larger than allowed
	if (maximum > RAND_MAX) {
		fprintf(stderr, "Maximum value must be lower than %d\n", RAND_MAX);
		exit(1);
	}


	// get the random number and print it
	int r = (rand() % maximum) + minimum;
	printf("%d\n", r);
	return 0;
}
